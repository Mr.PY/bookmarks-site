document.getElementById("myForm").addEventListener("submit", saveBookmark);
//On pressing Submitting running the below function to get the site values and store them in local storage
function saveBookmark(e) {
  var siteName = document.getElementById("siteName").value;
  var siteUrl = document.getElementById("siteUrl").value;

  if (!validateForm(siteName, siteUrl)) {
    return false;
  }

  var bookmark = {
    name: siteName,
    url: siteUrl
  };
  //Adding Data to Local Storage for further use.
  if (localStorage.getItem("bookmarks") === null) {
    var bookmarks = [];

    bookmarks.push(bookmark);

    localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  } else {
    var bookmarks = JSON.parse(localStorage.getItem("bookmarks"));

    bookmarks.push(bookmark);

    localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  }
  document.getElementById("myForm").reset();

  fetchBookmarks();
  //prevents form from submitting
  e.preventDefault();
}

//Add the Bookmark values to Html page.
function fetchBookmarks() {
  var bookmarks = JSON.parse(localStorage.getItem("bookmarks"));

  var bookmarkResults = document.getElementById("bookmarkResults");

  bookmarkResults.innerHTML = "";

  for (var i = 0; i < bookmarks.length; i++) {
    var name = bookmarks[i].name;
    var url = bookmarks[i].url;

    bookmarkResults.innerHTML +=
      '<div class="bookmarkBlock">' +
      '<h3 class="forGrid"> <p class="siteNames">' +
      name +
      "</p>" +
      ' <p class="goButton"><a target="_blank" href="' +
      url +
      '">GO</a></p> ' +
      ' <p class="deleteButton"><a onclick="deleteBookmark(\'' +
      url +
      '\')" href="#">Delete</a></p> ' +
      "</h3>" +
      "</div>";
  }
}

//Delete Bookmarks
function deleteBookmark(url) {
  var bookmarks = JSON.parse(localStorage.getItem("bookmarks"));

  for (var i = 0; i < bookmarks.length; i++) {
    if (bookmarks[i].url == url) {
      bookmarks.splice(i, 1);
    }
  }

  localStorage.setItem("bookmarks", JSON.stringify(bookmarks));

  fetchBookmarks();
}

// Validate Form
function validateForm(siteName, siteUrl) {
  if (!siteName || !siteUrl) {
    alert("Please fill in the form");
    return false;
  }
  return true;
}

// To get bookmarks on page load or refresh
window.onload = function() {
  fetchBookmarks();
};

// Form Toggle
document.getElementById("menuButton").addEventListener("click", toggleForm);
document.getElementById("myForm").classList.toggle("view");
function toggleForm(e) {
  document.getElementById("myForm").classList.toggle("view");
}
